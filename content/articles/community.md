---
title: Community
aliases:
    - /articles/IRC/
    - /articles/chat/
---

The [GitLab issue tracker](https://gitlab.com/signald/signald/-/issues) can be used to report bugs or get support. Additionally,
a matrix/IRC room exists for more quick questions or real time discussion:

* **IRC** server: `libera.chat` channel: `#signald` ([link](irc://irc.libera.chat/#signald))
* **Matrix** server: `libera.chat` channel: `#signald` ([link](https://matrix.to/#/#signald:libera.chat))
* **Web**: [link](https://web.libera.chat/#signald)