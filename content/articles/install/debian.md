# Debian Installation

put the following in `/etc/apt/sources.list.d/signald.list`:

```
deb https://updates.signald.org unstable main
```

And trust the signing key:

```
curl https://updates.signald.org/apt-signing-key.asc | sudo apt-key add -
```

Update the package list:

```
sudo apt update
```

Now you can install signald:

```
sudo apt install signald
```

Ensure the service is running:
```
sudo systemctl start signald
```

verify that the installation was successful:

```
signaldctl version
```

Other useful commands:

* see old logs: `journalctl -u signald`
* see new logs: `journalctl -fu signald` will show new logs as they appear, ctrl+c to stop
* check if signald is running: `systemctl status signald`
* restart signald: `sudo systemctl restart signald`